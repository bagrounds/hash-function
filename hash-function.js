/**
 * hash-function takes a string of any length and returns a 32 bit unsigned
 * integer.
 *
 * @module hash-function
 */
;(function () {
  'use strict'

  /* imports */
  var MersenneTwister = require('mersenne-twister')

  /* exports */
  module.exports = hashFunction

  var generator = new MersenneTwister()

  /**
   * hashFunction takes a string of any length and returns a 32 bit unsigned
   * integer.
   *
   * @function hashFunction
   * @alias hash-function
   *
   * @param {Object} options all function parameters
   * @param {String} options.string to compute hash function for
   * @param {Function} callback handle results
   */
  function hashFunction (options, callback) {
    var string = options.string

    var hashCode = hash(string)

    callback(null, hashCode)
  }

  /**
   * Simple, efficient, non-cryptographic (insecure) hash function
   *
   * @param {String} string to compute hash code for
   * @return {Number} hash code
   */
  function hash (string) {
    var hash = 0

    if (!string) return hash

    var i = 0

    while (i < string.length) {
      hash += (string.charCodeAt(i++) << 5)
    }

    return random(hash)
  }

  function random (number) {
    generator = new MersenneTwister(number)
    number = generator.random_int31()
    return number
  }
})()

