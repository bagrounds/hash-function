# hash-function

[![Travis-CI][travis-img]][travis-url]
[![Coveralls][coveralls-img]][coveralls-url]
[![Code Climate][codeclimate-img]][codeclimate-url]
[![Code Climate][codeclimate-issues-img]][codeclimate-issues-url]
[![js-standard-style][standard-img]][standard-url]

## Tests
``` bash
  $ npm test
```


## [Changelog][changelog-url]

## License
[MIT][license-url]


[changelog-url]: CHANGELOG.md

[license-url]: LICENSE

[standard-img]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg
[standard-url]: http://standardjs.com/

[travis-img]: https://img.shields.io/travis/bagrounds/hash-function/master.svg
[travis-url]: https://travis-ci.org/bagrounds/hash-function

[coveralls-img]: https://coveralls.io/repos/github/bagrounds/hash-function/badge.svg?branch=master
[coveralls-url]: https://coveralls.io/github/bagrounds/hash-function?branch=master

[codeclimate-img]: https://codeclimate.com/github/bagrounds/hash-function/badges/gpa.svg
[codeclimate-url]: https://codeclimate.com/github/bagrounds/hash-function

[codeclimate-issues-img]: https://codeclimate.com/github/bagrounds/hash-function/badges/issue_count.svg
[codeclimate-issues-url]: https://codeclimate.com/github/bagrounds/hash-function/issues

