/**
 * Tests for serve-function-module-template
 */
;(function () {
  /* global describe, it */
  'use strict'

  /* Imports */
  var expect = require('chai').expect

  var hash = require('../hash-function')

  /* Tests */
  describe('hash', function () {
    it('should not error with proper input', function (done) {
      var hashOptions = {
        string: 'test string'
      }

      hash(hashOptions, function (error, hash) {
        expect(error).to.not.be.ok
        expect(hash).to.be.a.number
        done()
      })
    })

    it('should error with improper input', function (done) {
      var hashOptions = {}

      hash(hashOptions, function (error, hash) {
        expect(error).to.be.an.error
        expect(hash).to.not.be.ok
        done()
      })
    })
  })
})()
