;(function () {
  'use strict'

  /* imports */
  var gulp = require('gulp')
  var gulpJsonEditor = require('gulp-json-editor')

  var TASK = {
    MAKE_GITLAB: {
      NAME: 'gitlab',
      SOURCE: '.gitlab-ci.yml',
      FUNCTION: makeGitlab,
      DEFAULT: true,
      WATCH: false
    }
  }

  var WATCH_TASKS = []
  var DEFAULT_TASKS = []

  // make an array of the above tasks
  var TASKS = Object.keys(TASK).map(function (taskKey) {
    return TASK[taskKey]
  })

  // create each gulp task
  TASKS.forEach(function (task) {
    if (task.DEFAULT) {
      DEFAULT_TASKS.push(task.NAME)
    }

    if (task.WATCH) {
      WATCH_TASKS.push(task)
    }

    gulp.task(task.NAME, task.FUNCTION)
  })

  gulp.task('default', DEFAULT_TASKS)

  gulp.task('watch', function watch () {
    WATCH_TASKS.forEach(function (task) {
      gulp.watch(task.SOURCE, [task.NAME])
    })
  })

  function makeGitlab () {
    return gulp.src(TASK.MAKE_GITLAB.SOURCE)
    .pipe(gulpJsonEditor(function (gitlab) {
      gitlab.variables.VERSION = require('./package.json').version
      return gitlab
    },
      {
        indent_char: ' ',
        indent_size: 2
      }))
    .pipe(gulp.dest('./'))
  }
})()

